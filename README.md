# cdetect

cDetect - Feature detection for portable C/C++ projects.  Based on the cDetect project at http://cdetect.sourceforge.net/  This is a fork with several added features including support for use when working with cross-compilers.
